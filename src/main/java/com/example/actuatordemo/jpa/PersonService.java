package com.example.actuatordemo.jpa;

import java.util.List;
import java.util.Optional;

import com.example.actuatordemo.web.models.Person;

public interface PersonService {
	/**
	 * 
	 * @param personId
	 * @return {@link Optional} {@link Person} objects if present in database
	 *         for supplied person ID
	 */
	public Optional<Person> getPersonById(int personId);
	/**
	 * 
	 * @return {@link List} of {@link Person} model class fo rall available
	 *         entities
	 */
	public List<Person> getAllPersons();
	/**
	 * 
	 * @param personId
	 * @return Delete the person from database for supplied id
	 */
	public boolean removePerson(int personId);
	/**
	 * 
	 * @param person
	 * @return {@link Optional} {@link Person} objects after save or update Save
	 *         if no personId present else update
	 */
	public Optional<Person> saveUpdatePerson(Person person);

}
